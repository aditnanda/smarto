package adcdev.simrev.smarto;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Timer;
import java.util.TimerTask;


public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private ImageView toolbarTitle ;
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private ImageButton nelongso,miesetan, burgerking, yoshinoya, rika, nelongsoC, pepper, piola, nasibebek, nasigoreng, esteh,esjeruk;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        viewPager = view.findViewById(R.id.viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity());
        viewPager.setAdapter(viewPagerAdapter);

        sliderDotspanel = view.findViewById(R.id.SliderDots);
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.nonactive_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            sliderDotspanel.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MyTimerTask(), 2000, 4000);

        nelongso = view.findViewById(R.id.nelongso);
        nelongso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "RESTORAN");
                arguments.putString("icon","nelongso");
                arguments.putString("nama","Nelongso");
                arguments.putString("adaCatering","ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        miesetan = view.findViewById(R.id.miesetan);
        miesetan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "RESTORAN");
                arguments.putString("icon","miesetan");
                arguments.putString("nama","Mie Setan");
                arguments.putString("adaCatering","tidak ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        burgerking = view.findViewById(R.id.burgerking);
        burgerking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "RESTORAN");
                arguments.putString("icon","burgerking");
                arguments.putString("nama","Burger King");
                arguments.putString("adaCatering","tidak ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        yoshinoya = view.findViewById(R.id.yoshinoya);
        yoshinoya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "RESTORAN");
                arguments.putString("icon","yoshinoya");
                arguments.putString("nama","Yoshinoya");
                arguments.putString("adaCatering","tidak ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        rika = view.findViewById(R.id.rika);
        rika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "CATERING");
                arguments.putString("icon","rika");
                arguments.putString("nama","Rika");
                arguments.putString("adaCatering","tidak ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        nelongsoC = view.findViewById(R.id.nelongsoC);
        nelongsoC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "CATERING");
                arguments.putString("icon","nelongso");
                arguments.putString("nama","Nelongso");
                arguments.putString("adaCatering","tidak ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        pepper = view.findViewById(R.id.pepper);
        pepper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "CATERING");
                arguments.putString("icon","pepper");
                arguments.putString("nama","Pepper");
                arguments.putString("adaCatering","tidak ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        piola = view.findViewById(R.id.piola);
        piola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle arguments = new Bundle();
                arguments.putString( "judul" , "CATERING");
                arguments.putString("icon","piola");
                arguments.putString("nama","Piola");
                arguments.putString("adaCatering","tidak ada");
                Fragment fragment = new NelongsoFragment();
                fragment.setArguments(arguments);
                loadFragment(fragment);
            }
        });

        nasibebek = view.findViewById(R.id.nasibebek);
        nasibebek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),MakananDipilih.class);
                intent.putExtra( "judul" , "RESTORAN");
                intent.putExtra("icon","nelongso");
                intent.putExtra("nama","Nelongso");
                intent.putExtra("deskripsi"," yang bahannya dipilih melalui tahap seleksi yang panjang sehingga memiliki kualitas yang sempurna");
                intent.putExtra("harga", "Rp. 15.000");
                intent.putExtra("namamakanan","Nasi Bebek Goreng Spesial");
                intent.putExtra("iconmakanan","nasibebekgorengspesial");
                startActivity(intent);
            }
        });

        nasigoreng = view.findViewById(R.id.nasigoreng);
        nasigoreng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),MakananDipilih.class);
                intent.putExtra( "judul" , "CATERING");
                intent.putExtra("icon","rika");
                intent.putExtra("nama","Rika");
                intent.putExtra("deskripsi"," yang bahannya dipilih melalui tahap seleksi yang panjang sehingga memiliki kualitas yang sempurna");
                intent.putExtra("harga", "Rp. 17.500");
                intent.putExtra("namamakanan","Nasi Goreng Udang Spesial");
                intent.putExtra("iconmakanan","nasigorengspesial");
                startActivity(intent);
            }
        });

        esteh = view.findViewById(R.id.esteh);
        esteh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),MakananDipilih.class);
                intent.putExtra( "judul" , "CATERING");
                intent.putExtra("icon","rika");
                intent.putExtra("nama","Rika");
                intent.putExtra("deskripsi"," yang bahannya dipilih melalui tahap seleksi yang panjang sehingga memiliki kualitas yang sempurna");
                intent.putExtra("harga", "Rp. 3.000");
                intent.putExtra("namamakanan","ES Teh Manis");
                intent.putExtra("iconmakanan","esteh");
                startActivity(intent);
            }
        });

        esjeruk = view.findViewById(R.id.esjeruk);
        esjeruk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),MakananDipilih.class);
                intent.putExtra( "judul" , "CATERING");
                intent.putExtra("icon","pepper");
                intent.putExtra("nama","Pepper");
                intent.putExtra("deskripsi"," yang bahannya dipilih melalui tahap seleksi yang panjang sehingga memiliki kualitas yang sempurna");
                intent.putExtra("harga", "Rp. 3.500");
                intent.putExtra("namamakanan","ES Jeruk");
                intent.putExtra("iconmakanan","esjeruk");
                startActivity(intent);
            }
        });

        return view;


    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {

            HomeFragment.this.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(viewPager.getCurrentItem() == 0){
                        viewPager.setCurrentItem(1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }

                }
            });

        }
    }

}
