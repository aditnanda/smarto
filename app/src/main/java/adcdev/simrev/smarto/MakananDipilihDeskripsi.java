package adcdev.simrev.smarto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;


public class MakananDipilihDeskripsi extends AppCompatActivity {

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private boolean flag = false;
    private ImageView nasibebek, iconImage;
    private TextView namaRest,harga_tv,namamakanan_tv, deskripsi_tv;
    private Button catering;
    private TabLayout tab;
    private volatile boolean cancelled = false;
    private Spinner pengiriman;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makanan_dipilih_deskripsi);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        viewPager = findViewById(R.id.viewPager);

        ViewPagerAdapterNasiBebek viewPagerAdapter = new ViewPagerAdapterNasiBebek(getApplicationContext());
        viewPager.setAdapter(viewPagerAdapter);

        final String namamakanan = getIntent().getStringExtra("namamakanan");
        final String icon = getIntent().getStringExtra("icon");
        final String harga = getIntent().getStringExtra("harga");
        final String deskripsi = getIntent().getStringExtra("deskripsi");
        final String judul = getIntent().getStringExtra("judul");

        final String nama = getIntent().getStringExtra("nama");
        final String iconmakanan = getIntent().getStringExtra("iconmakanan");

        sliderDotspanel = findViewById(R.id.SliderDots);
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){
            dots[i] = new ImageView(getApplicationContext());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            sliderDotspanel.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new MakananDipilihDeskripsi.MyTimerTask(), 2000, 4000);
        tab = findViewById(R.id.tabs_deskripsi);
        TabLayout.Tab tabs = tab.getTabAt(1);
        tabs.select();



        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    Intent intent = new Intent(getApplicationContext(),MakananDipilih.class);
                    intent.putExtra( "judul" , judul);
                    intent.putExtra("icon",icon);
                    intent.putExtra("nama",nama);
                    intent.putExtra("deskripsi",deskripsi);
                    intent.putExtra("harga", harga);
                    intent.putExtra("namamakanan",namamakanan);
                    intent.putExtra("iconmakanan",iconmakanan);
                    startActivity(intent);
                } else if (tab.getPosition() == 1) {

                } else if (tab.getPosition() == 2) {

                } else {

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        this.setTitle(judul);

        iconImage = findViewById(R.id.icon);
        namaRest = findViewById(R.id.namaRest);
        catering = findViewById(R.id.catering);
        namamakanan_tv = findViewById(R.id.namamakanan);
        harga_tv = findViewById(R.id.harga);

        Context c = getApplicationContext();
        int id = c.getResources().getIdentifier("drawable/"+icon, null, c.getPackageName());
        iconImage.setImageResource(id);

        namaRest.setText(nama);
        namamakanan_tv.setText(namamakanan);
        harga_tv.setText(harga);

        pengiriman = (Spinner) findViewById(R.id.pengiriman);

        //String mPengiriman = pengiriman.getSelectedItem().toString();

        deskripsi_tv = findViewById(R.id.deskripsi);
        deskripsi_tv.setText(namamakanan+deskripsi);

    }

    public class MyTimerTask extends TimerTask {

        @Override
        public void run() {
            if ( cancelled ) {
                return; // or handle this however you want
            }else {
                MakananDipilihDeskripsi.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (viewPager.getCurrentItem() == 0) {
                            viewPager.setCurrentItem(1);
                        } else {
                            viewPager.setCurrentItem(0);
                        }

                    }
                });
            }

        }
    }


    @Override
    public void onBackPressed() {
        cancelled = true;
        finish();
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.saveData){
            Toast.makeText(getApplicationContext(), "Fitur ini masih Coming Soon", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
