package adcdev.simrev.smarto;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;




public class NelongsoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private boolean flag = false;
    private ImageView nasibebek, nasigoreng, iconImage;
    private TextView namaRest;
    private Button catering;
    private TabLayout tab;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public NelongsoFragment() {
        // Required empty public constructor
    }

    public static NelongsoFragment newInstance(String param1, String param2) {
        NelongsoFragment fragment = new NelongsoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nelongso, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        //viewPager = findViewById(R.id.viewPager);


        Bundle arguments = getArguments();
        final String judul = arguments.getString("judul");
        final String icon = arguments.getString("icon");
        final String nama = arguments.getString("nama");
        final String adaCatering = arguments.getString("adaCatering");

        ((MainActivity) getActivity())
                .setActionBarTitle(judul);

        iconImage = view.findViewById(R.id.icon);
        namaRest = view.findViewById(R.id.namaRest);
        catering = view.findViewById(R.id.catering);

        Context c = getActivity().getApplicationContext();
        int id = c.getResources().getIdentifier("drawable/"+icon, null, c.getPackageName());
        iconImage.setImageResource(id);

        namaRest.setText(nama);

        if(adaCatering == "ada"){
            catering.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle arguments = new Bundle();
                    arguments.putString( "judul" , "CATERING");
                    arguments.putString("icon",icon);
                    arguments.putString("nama",nama);
                    arguments.putString("adaCatering","tidakada");
                    Fragment fragment = new NelongsoFragment();
                    fragment.setArguments(arguments);
                    loadFragment(fragment);
                }
            });

            catering.setVisibility(View.VISIBLE);
        }else {
            catering.setVisibility(View.GONE);
        }

        viewPager = view.findViewById(R.id.viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity());
        viewPager.setAdapter(viewPagerAdapter);

        sliderDotspanel = view.findViewById(R.id.SliderDots);
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.nonactive_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            sliderDotspanel.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tab = view.findViewById(R.id.tabs);


        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                } else if (tab.getPosition() == 1) {
                    Bundle arguments = new Bundle();
                    arguments.putString( "judul" , judul);
                    arguments.putString("icon",icon);
                    arguments.putString("nama",nama);
                    arguments.putString("adaCatering",adaCatering);
                    Fragment fragment = new NelongsominumFragment();
                    fragment.setArguments(arguments);
                    loadFragment(fragment);
                } else {
                    Bundle arguments = new Bundle();
                    arguments.putString( "judul" , judul);
                    arguments.putString("icon",icon);
                    arguments.putString("nama",nama);
                    arguments.putString("adaCatering",adaCatering);
                    Fragment fragment = new NelongsoprofilFragment();
                    fragment.setArguments(arguments);
                    loadFragment(fragment);
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        nasibebek = view.findViewById(R.id.nasibebek);
        nasibebek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),MakananDipilih.class);
                intent.putExtra( "judul" , judul);
                intent.putExtra("icon",icon);
                intent.putExtra("nama",nama);
                intent.putExtra("deskripsi"," yang bahannya dipilih melalui tahap seleksi yang panjang sehingga memiliki kualitas yang sempurna");
                intent.putExtra("harga", "Rp. 15.000");
                intent.putExtra("namamakanan","Nasi Bebek Goreng Spesial");
                intent.putExtra("iconmakanan","nasibebekgorengspesial");
                startActivity(intent);
            }
        });

        nasigoreng = view.findViewById(R.id.nasigoreng);
        nasigoreng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),MakananDipilih.class);
                intent.putExtra( "judul" , judul);
                intent.putExtra("icon",icon);
                intent.putExtra("nama",nama);
                intent.putExtra("deskripsi"," yang bahannya dipilih melalui tahap seleksi yang panjang sehingga memiliki kualitas yang sempurna");
                intent.putExtra("harga", "Rp. 17.500");
                intent.putExtra("namamakanan","Nasi Goreng Udang Spesial");
                intent.putExtra("iconmakanan","nasigorengspesial");
                startActivity(intent);
            }
        });


        return view;
    }


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
