package adcdev.simrev.smarto;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class NelongsoprofilFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private boolean flag = false;
    private ImageView nasibebek, iconImage, imageprofil;
    private TextView namaRest,textdeskripsi;
    private Button catering;
    private TabLayout tab;



    /**
     * The {@link ViewPager} that will host the section contents.
     */
    public NelongsoprofilFragment() {
        // Required empty public constructor
    }

    public static NelongsoprofilFragment newInstance(String param1, String param2) {
        NelongsoprofilFragment fragment = new NelongsoprofilFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nelongsoprofil, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);

        //viewPager = findViewById(R.id.viewPager);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        TabLayout.Tab tabs = tabLayout.getTabAt(2);
        tabs.select();


        Bundle arguments = getArguments();
        final String judul = arguments.getString("judul");
        final String icon = arguments.getString("icon");
        final String nama = arguments.getString("nama");
        final String adaCatering = arguments.getString("adaCatering");

        ((MainActivity) getActivity())
                .setActionBarTitle(judul);

        iconImage = view.findViewById(R.id.icon);
        namaRest = view.findViewById(R.id.namaRest);
        textdeskripsi = view.findViewById(R.id.textdeskripsi);
        catering = view.findViewById(R.id.catering);
        imageprofil = view.findViewById(R.id.imageprofil);

        Context c = getActivity().getApplicationContext();
        int id = c.getResources().getIdentifier("drawable/"+icon, null, c.getPackageName());
        iconImage.setImageResource(id);
        imageprofil.setImageResource(id);

        namaRest.setText(nama);
        textdeskripsi.setText("\""+nama+" adalah sebuah "+judul+" yang menyediakan berbagai makanan dan minuman"+"\"");

        if(adaCatering == "ada"){
            catering.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle arguments = new Bundle();
                    arguments.putString( "judul" , "CATERING");
                    arguments.putString("icon",icon);
                    arguments.putString("nama",nama);
                    arguments.putString("adaCatering","tidakada");
                    Fragment fragment = new NelongsoprofilFragment();
                    fragment.setArguments(arguments);
                    loadFragment(fragment);
                }
            });

            catering.setVisibility(View.VISIBLE);
        }else {
            catering.setVisibility(View.GONE);
        }

        viewPager = view.findViewById(R.id.viewPager);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity());
        viewPager.setAdapter(viewPagerAdapter);

        sliderDotspanel = view.findViewById(R.id.SliderDots);
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for(int i = 0; i < dotscount; i++){
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.nonactive_dot));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(8, 0, 8, 0);
            sliderDotspanel.addView(dots[i], params);
        }

        dots[0].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.active_dot));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for(int i = 0; i< dotscount; i++){
                    dots[i].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tab = view.findViewById(R.id.tabs);


        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    Bundle arguments = new Bundle();
                    arguments.putString( "judul" , judul);
                    arguments.putString("icon",icon);
                    arguments.putString("nama",nama);
                    arguments.putString("adaCatering",adaCatering);
                    Fragment fragment = new NelongsoFragment();
                    fragment.setArguments(arguments);
                    loadFragment(fragment);
                } else if (tab.getPosition() == 1) {
                    Bundle arguments = new Bundle();
                    arguments.putString( "judul" , judul);
                    arguments.putString("icon",icon);
                    arguments.putString("nama",nama);
                    arguments.putString("adaCatering",adaCatering);
                    Fragment fragment = new NelongsominumFragment();
                    fragment.setArguments(arguments);
                    loadFragment(fragment);
                } else {
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return view;
    }


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
