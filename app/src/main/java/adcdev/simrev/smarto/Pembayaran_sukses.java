package adcdev.simrev.smarto;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class Pembayaran_sukses extends AppCompatActivity {

    /**
     * The {@link ViewPager} that will host the section contents.
     */

    private ImageView nasibebek, iconpengiriman_iv,iconpembayaran_iv;

    private volatile boolean cancelled = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pembayaran_sukses);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String namamakanan = getIntent().getStringExtra("namamakanan");
        final String icon = getIntent().getStringExtra("icon");
        final String harga = getIntent().getStringExtra("harga");



        final String judul = getIntent().getStringExtra("judul");

        final String nama = getIntent().getStringExtra("nama");
        final String iconmakanan = getIntent().getStringExtra("iconmakanan");
        final String mPengiriman = getIntent().getStringExtra("mPengiriman");
        final String quantity = getIntent().getStringExtra("quantity");
        final String iconpembayaran = getIntent().getStringExtra("iconpembayaran");


        this.setTitle("PEMBAYARAN");

        iconpembayaran_iv = findViewById(R.id.iconpembayaran);
        Context c = getApplicationContext();
        int id = c.getResources().getIdentifier("drawable/"+iconpembayaran, null, c.getPackageName());
        iconpembayaran_iv.setImageResource(id);

        iconpengiriman_iv = findViewById(R.id.iconpengiriman);
        Context cr = getApplicationContext();
        int idr = cr.getResources().getIdentifier("drawable/"+mPengiriman, null, c.getPackageName());
        iconpengiriman_iv.setImageResource(idr);

    }


    @Override
    public void onBackPressed() {
        cancelled = true;
        finish();
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.saveData){
            Toast.makeText(getApplicationContext(), "Fitur ini masih Coming Soon", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
