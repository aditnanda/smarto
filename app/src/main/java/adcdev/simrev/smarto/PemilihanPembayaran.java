package adcdev.simrev.smarto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;


public class PemilihanPembayaran extends AppCompatActivity {

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private boolean flag = false;
    private ImageView nasibebek, pilihimg,restpembayaran;
    private TextView hargapembayaran,namapembayaran,namamakanan_tv;
    private Button catering, pesan;
    private TabLayout tab;
    private volatile boolean cancelled = false;
    private Spinner pengiriman;


    private CardView indomaret_cv, alfamart_cv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemilihanpembayaran);

        Toolbar toolbar =  findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String namamakanan = getIntent().getStringExtra("namamakanan");
        final String icon = getIntent().getStringExtra("icon");
        final String harga = getIntent().getStringExtra("harga");



        final String judul = getIntent().getStringExtra("judul");

        final String nama = getIntent().getStringExtra("nama");
        final String iconmakanan = getIntent().getStringExtra("iconmakanan");
        final String mPengiriman = getIntent().getStringExtra("mPengiriman");
        final String quantity = getIntent().getStringExtra("quantity");


        this.setTitle("PILIH METODE PEMBAYARAN");

        pilihimg = findViewById(R.id.pilihimg);
        Context c = getApplicationContext();
        int id = c.getResources().getIdentifier("drawable/"+iconmakanan, null, c.getPackageName());
        pilihimg.setImageResource(id);

        indomaret_cv = findViewById(R.id.indomaret);
        alfamart_cv = findViewById(R.id.alfamart);

        namapembayaran = findViewById(R.id.namapembayaran);
        namapembayaran.setText(namamakanan);

        hargapembayaran = findViewById(R.id.hargapembayaran);
        hargapembayaran.setText(quantity+" x "+harga);

        restpembayaran = findViewById(R.id.restpembayaran);
        Context cr = getApplicationContext();
        int idr = cr.getResources().getIdentifier("drawable/"+icon, null, c.getPackageName());
        restpembayaran.setImageResource(idr);

        indomaret_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Pembayaran.class);
                intent.putExtra( "judul" , judul);
                intent.putExtra("icon",icon);
                intent.putExtra("nama",nama);
                intent.putExtra("harga", harga);
                intent.putExtra("namamakanan",namamakanan);
                intent.putExtra("iconmakanan",iconmakanan);
                intent.putExtra("mPengiriman",mPengiriman);
                intent.putExtra("quantity",quantity);
                intent.putExtra("iconpembayaran","indomaret");
                startActivity(intent);
            }
        });

        alfamart_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),Pembayaran.class);
                intent.putExtra( "judul" , judul);
                intent.putExtra("icon",icon);
                intent.putExtra("nama",nama);
                intent.putExtra("harga", harga);
                intent.putExtra("namamakanan",namamakanan);
                intent.putExtra("iconmakanan",iconmakanan);
                intent.putExtra("mPengiriman",mPengiriman);
                intent.putExtra("quantity",quantity);
                intent.putExtra("iconpembayaran","alfamart");
                startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        cancelled = true;
        finish();
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.saveData){
            Toast.makeText(getApplicationContext(), "Fitur ini masih Coming Soon", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
